package ru.inside.messages.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.inside.messages.models.Message;

public interface MessageRepository extends CrudRepository<Message, Long> {
}
