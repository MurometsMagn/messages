package ru.inside.messages.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.inside.messages.models.User;

public interface UserRepository extends CrudRepository<User, Long> {

    User findByLogin(String login);
}
