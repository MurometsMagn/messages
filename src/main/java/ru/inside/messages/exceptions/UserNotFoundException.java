package ru.inside.messages.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND) //404
public class UserNotFoundException extends Exception {

    public UserNotFoundException(String text) {
        super(text);
    }

    public UserNotFoundException(String text, Throwable cause) {
        super(text, cause);
    }
}
