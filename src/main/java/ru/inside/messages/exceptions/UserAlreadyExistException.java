package ru.inside.messages.exceptions;

public class UserAlreadyExistException extends Exception {
    public UserAlreadyExistException(String text) {
        super(text);
    }
}
