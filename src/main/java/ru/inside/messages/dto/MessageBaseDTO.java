package ru.inside.messages.dto;

import ru.inside.messages.models.Message;

public class MessageBaseDTO {
    String text;
    int authorId;

    public static MessagePostDTO fromEntity(Message message) {
        return new MessagePostDTO(message.getText(),
                message.getAuthor().getId());
    }
}
