package ru.inside.messages.dto;

import ru.inside.messages.models.Message;
import ru.inside.messages.models.User;

import java.util.List;

public class UserPostDTO {
    private String login;
    private List<Message> messages;

    public UserPostDTO(String login, List<Message> messages) {
        this.login = login;
        this.messages = messages;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public static UserPostDTO fromEntity(User user) {
        return new UserPostDTO(user.getLogin(),
                //userEntity.getMessages().stream().map((UserPostDTO::fromEntity).collect(Collectors.toList())))
                user.getMessages());
    }
}
