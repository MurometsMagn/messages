package ru.inside.messages.dto;

import ru.inside.messages.models.Message;

public class MessagePostDTO{
    private String text;
    private int authorId;

    public MessagePostDTO(String text, int authorId) {
        this.text = text;
        this.authorId = authorId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public static MessagePostDTO fromEntity(Message message) {
        return new MessagePostDTO(message.getText(),
                message.getAuthor().getId());
    }
}
