package ru.inside.messages.dto;


public class MessageGetDTO extends MessagePostDTO{
    private int id;

    public MessageGetDTO(int id, String text, int authorId) {
        super(text, authorId);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

//todo: 30.11. 21 hometask
/*
  1.Создать UserGetDTO И UserPostDTO
  2. в контроллерах заменить использование entity на DTO
 */