package ru.inside.messages.dto;;

import ru.inside.messages.models.Message;

import java.util.List;

public class UserGetDTO extends UserPostDTO{
    private int id;

    public UserGetDTO(String login, List<Message> messages, int id) {
        super(login, messages);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
