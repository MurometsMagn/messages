package ru.inside.messages.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.inside.messages.models.Message;
import ru.inside.messages.services.MessageService;

@RestController
@RequestMapping("/messages")
public class MessageController {
    @Autowired
    private MessageService messageService;

    @PostMapping
    public ResponseEntity sendMessage(@RequestBody Message message,
                                      @RequestParam Long user_Id) {
        try {
            return ResponseEntity.ok(messageService.sendMessage(message, user_Id));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка");
        }
    }

    @PutMapping
    public ResponseEntity receiveMessage(@RequestBody Message message,
                                         @RequestParam Long user_id) {
        try {
            return ResponseEntity.ok(messageService.receiveMessage((long) message.getId(), user_id));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка");
        }
    }

//    @PutMapping
//    public ResponseEntity receiveMessage(@RequestParam Long id) {
//        try {
//            return ResponseEntity.ok(messageService.receiveMessage(id));
//        } catch (Exception e) {
//            return ResponseEntity.badRequest().body("Произошла ошибка");
//        }
//    }
}
