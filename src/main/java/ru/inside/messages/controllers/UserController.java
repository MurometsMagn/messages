package ru.inside.messages.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.inside.messages.dto.LoginDTO;
import ru.inside.messages.services.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    public LoginDTO login() {
        return null;
        //todo:
        /*
        1. вызвать метод login из UserService
        2. если сгенерированна ислюч. ситуация - вернуть код 401 (не авторизован)
        3. в противном случае - вернуть полученный loginDTO
         */
    }
}
