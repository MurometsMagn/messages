package ru.inside.messages.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.inside.messages.models.Message;
import ru.inside.messages.models.User;
import ru.inside.messages.repositories.MessageRepository;
import ru.inside.messages.repositories.UserRepository;

@Service
public class MessageService {
    @Autowired
    private MessageRepository messageRepo;
    @Autowired
    private UserRepository userRepo;

    public Message sendMessage(Message message, Long user_id) {
        User user = userRepo.findById(user_id).get();
        message.setAuthor(user);
        return messageRepo.save(message);
    }

    public Message receiveMessage(Long id) {
        Message message = messageRepo.findById(id).get();
        message.setText("txt");
        return message;
    }

    public Message receiveMessage(Long message_id, Long user_id) {
        User user = userRepo.findById(user_id).get();
        Message message = user.getMessages().get(Math.toIntExact((message_id)));
        //message.setUserEntity(user);
        return messageRepo.save(message);
    }

//    public static Message toModel(MessageEntity entity) {
//        return new Message(entity.getId(),
//                entity.getAuthor().getId(),
//                entity.getText());
//    }
}
