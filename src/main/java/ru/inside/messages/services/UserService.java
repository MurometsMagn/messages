package ru.inside.messages.services;

//для регистрации и авторизации пользователей

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.inside.messages.models.User;
import ru.inside.messages.exceptions.UserAlreadyExistException;
import ru.inside.messages.exceptions.UserNotFoundException;
import ru.inside.messages.dto.LoginDTO;
import ru.inside.messages.repositories.UserRepository;

import java.util.NoSuchElementException;

@Service
public class UserService {
    //private final UserRepository userRepository;
    @Autowired
    private UserRepository userRepo;


//    public UserService(UserRepository userRepository) {
//        this.userRepository = userRepository;
//    }

    public LoginDTO login(String login, String password) {
        return null;
        //todo:
        /*
        1. вызвать метод userRepository по поиску пользователя
        2. получить хеш пароля при помощи bcrypt и сохраненного хеша пользователя
        3. если хэш совпадает - создать jwt, поместить его в loginDTO и вернуть
        4. в противном случае - сгенерировать исключительную ситуацию
         */
    }

    public User registration(User user) throws UserAlreadyExistException {
        if (userRepo.findByLogin(user.getLogin()) != null) {
            throw new UserAlreadyExistException("Пользователь с таким именем существует");
        }
        return userRepo.save(user);
    }

    public User getOne(Long id) throws UserNotFoundException {
        try {
            return userRepo.findById(id).get();
        } catch (NoSuchElementException e) {
            throw new UserNotFoundException("Пользователь не найден");
        }
    }

    public Long delete(Long id) {
        userRepo.deleteById(id);
        return id;
    }

//    public static User toModel(UserEntity entity) {
//        return new User(entity.getId(),
//                entity.getLogin(),
//                entity.getMessages().stream().map(MessageService::toModel).collect(Collectors.toList()));
//    }
}
