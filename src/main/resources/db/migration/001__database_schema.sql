CREATE TABLE users (
    id       SERIAL       PRIMARY KEY,
    login    VARCHAR (64) UNIQUE,
    password VARCHAR (64)
);

CREATE TABLE messages (
    id        SERIAL  PRIMARY KEY,
    author_id INTEGER REFERENCES users (id) ON DELETE SET NULL,
    text      TEXT
);
